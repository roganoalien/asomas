var MAIN = (function(){
	var _$acordeon_item,
		_$body;
	var _initVars = function _initVars(){
		_$acordeon_item = $('.acordeon-item');
		_$body = $('body');
	};
	var _initEvents = function _initEvents(){
		_$body.delegate('.acordeon-item', 'click', function(){
			_$acordeon_item.removeClass('active');
			$(this).addClass('active');
		});
	};
	var _initHome = function _initHome(){};
	return{
		init : function init(){
			_initVars();
			_initEvents();
		},
		home : function home(){
			_initHome();
		},
		menu : function menu(){}
	}
})();