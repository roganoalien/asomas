<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="description" content="<?php bloginfo('description'); ?>">
	<meta name="robots" content="index, follow">
	<meta name="author" content="Rodrigo García">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
	<title><?php bloginfo('name'); ?> | Inicio</title>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/libs/materialize.css">
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
	<!-- FACEBOOK CARD -->
	<meta property="og:type" content="website">
	<meta property="og:title" content="Rodrigo García Blog">
	<meta property="og:site_name" content="<?php bloginfo('name'); ?>">
	<meta property="og:url" content="<?php bloginfo('url'); ?>">
	<meta property="og:description" content="<?php bloginfo('description'); ?>">
	<meta property="fb:app_id" content="641155919373999">
	<meta property="og:image" content="<?php echo get_template_directory_uri (); ?>/img/twitter_card.png" />
	<!-- TWITTER CARD -->
	<meta name="twitter:card" content="app">
	<meta name="twitter:site" content="@roganoalien">
	<meta name="twitter:title" content="<?php bloginfo('name'); ?>">
	<meta name="twitter:description" content="<?php bloginfo('description'); ?>">
	<meta name="twitter:image:src" content="<?php echo get_template_directory_uri(); ?>/img/twitter_card.png">
</head>
<body>
	<nav class="le-navigation col s12">
		<div class="container">
			<div class="row">
				<a class="col s12 m3" href="<?php echo get_site_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-lr.png" alt="Logo ASOMAS"></a>
				<?php /**
					* Displays a navigation menu
					* @param array $args Arguments
					*/
					$args = array(
						'theme_location' => 'header-menu',
						'menu' => 'header-menu',
						'container' => 'div',
						'container_class' => 'main-menu col s12 m9',
						'container_id' => '',
						'menu_class' => 'menu',
						'menu_id' => '',
						'echo' => true,
						'fallback_cb' => 'wp_page_menu',
						'items_wrap' => '<ul id = "%1$s" class = "%2$s ">%3$s</ul>',
						'depth' => 0,
						'walker' => ''
					);
				
					wp_nav_menu( $args ); ?>
			</div>
		</div>
	</nav>
	<div class="asomas-container">