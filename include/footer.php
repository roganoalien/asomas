<?php
	$args_footer_logos = array(
		'category_name' => 'footer-logos',
		'posts_per_page' => 1
	);
?>
		<footer class="col s12 main-footer" style="background: url(<?php echo get_template_directory_uri(); ?>/img/patrocinadores-bg.jpg)no-repeat center center; background-size:cover;">
			<div class="footer-logos col s12">
				<div class="container">
			    	<div class="row">
			    	    <?php query_posts ($args_footer_logos); ?>
			    	    <?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
							<div class="footer-logos-title">
			    	        	<h2><?php the_title(); ?></h3>            
			    	        </div>
			    	        <div class="footer-logos-images">
			    	        	<div><?php the_content(); ?></div>            
			    	    	</div>
			    	    <?php endwhile; ?>
			    	    <?php else: ?>
			    	    	<p>NO EXISTE NINGÚN POST</p>
			    	    <?php endif; ?>
			    		<?php rewind_posts(); ?>
			    	</div>
		        </div>
		    </div>
		    <div class="footer-newsletter col s12">
		    	<div class="container">
		    		<div class="row">
		    			<div class="newsletter-form col s12 l3 offset-l3">
		    				<h4>Suscríbete para recibir noticias</h4>
		    				<form action="">
		    					<input type="text" placeholder="Nombre">
		    					<input type="email" placeholder="Email">
		    					<button class="btn brown-btn" type="submit">Suscribirse</button>
		    				</form>
		    			</div>
		    			<div class="newsletter-donations col s12 l3">
		    				<a href="<?php echo site_url(); ?>/apoyo" class="donations btn asomas-btn-2">DONACIONES</a>
        					<a href="" class="donations btn asomas-btn-2">VIDEO</a>
		    			</div>
		    		</div>
		    	</div>
		    </div>
		    <div class="footer-legal">
		    	<div class="container">
		    		<div class="row">
		    			<p>
		    				<b>Contacto</b><br>
		    				<a href="mailto:contacto@asomas.org.mx">contacto@asomas.org.mx</a><br>
		    				(55) 53957679<br>
		    				(55) 55803397 <br>
							Ext 101<br>
							<a href="https://www.facebook.com/Asomas-IAP-127976263916799/" target="_blank">Facebook</a><br>
							Retorno Miguel Lanz Duret # 57<br>
							Col. Periodista<br>
							Del. Miguel Hidalgo<br>
							CP: 11220<br>
							RFC: AMA0111218G0<br>
							<a href="https://www.google.com.mx/maps/place/Rtno.+Lic.+Miguel+Lanz+Duret+57,+Periodista,+11220+Ciudad+de+M%C3%A9xico,+CDMX/@19.4455877,-99.2213794,17z/data=!3m1!4b1!4m5!3m4!1s0x85d2023bc17588d9:0x9c0f644287597c71!8m2!3d19.4455877!4d-99.2191854?hl=es" target="_blank">Ver en Google Maps</a>
		    			</p>
		    		</div>
		    	</div>
		    </div>
		</footer>
	</div>
	<script src="<?php echo get_template_directory_uri(); ?>/js/libs/jquery-3.1.0.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
	<script>
		$(document).ready(function(){
			MAIN.init();
		});
	</script>
</body>
</html>