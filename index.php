<?php get_template_part('include/header'); ?>
<?php
	$args_landing = array(
		'category_name' => 'index-landing',
		'posts_per_page' => 1
	);
	$args_historia = array(
		'category_name' => 'index-historia',
		'posts_per_page' => 1
	);
	$args_acordeon = array(
		'category_name' => 'index-acordeon',
		'posts_per_page' => 3,
		'order' => 'ASC'
	);
    $count = 0;
?>
<?php query_posts ($args_landing); ?>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
        <?php $thumb = wp_get_attachment_url( get_post_thumbnail_id($post->ID)); ?>
        <section class="index-landing col s12" style="background: url(<?php echo $thumb ?>);">
        	<div class="container">
        		<div class="row">
        			<article class="col s12">
        				<h1><?php the_title(); ?></h1>
        				<p><?php the_content(); ?></p>
        				<div class="the-buttons">
        					<a href="<?php echo site_url(); ?>/apoyo" class="donations btn asomas-btn">DONACIONES</a>
        					<a href="" class="donations btn asomas-btn">VIDEO</a>
        				</div>
        			</article>
        		</div>
        	</div>
        </section>
    <?php endwhile; ?>
    <?php else: ?>
    	<p>NO EXISTE NINGÚN POST</p>
	<?php endif; ?>
<?php rewind_posts(); ?>
<?php query_posts ($args_historia); ?>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
        <?php $thumb = wp_get_attachment_url( get_post_thumbnail_id($post->ID)); ?>
        <section class="index-historia col s12" style="background: url(<?php echo $thumb ?>);">
        	<div class="container">
        		<div class="row">
        			<article class="col s12">
        				<h2><?php the_title(); ?></h2>
        				<div><?php the_content(); ?></div>
        			</article>
        		</div>
        	</div>
        </section>
    <?php endwhile; ?>
    <?php else: ?>
    	<p>NO EXISTE NINGÚN POST</p>
	<?php endif; ?>
<?php rewind_posts(); ?>
<section class="index-acordeon col s12" style="background: url(<?php echo get_template_directory_uri(); ?>/img/mision-vision-bg.jpg)no-repeat center center">
	<div class="container">
		<div class="row">
			<?php query_posts ($args_acordeon); ?>
			    <?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
			        <?php $thumb = wp_get_attachment_url( get_post_thumbnail_id($post->ID)); ?>
                    <?php if($count < 1){?>
                        <article class="acordeon-item col s12 active">
                    <?php } else { ?>
			        <article class="acordeon-item col s12">
                    <?php } $count++;?>

			        	<div class="acordeon-title">
                            <h3><?php the_title(); ?></h3>            
                        </div>
			        	<div class="acordeon-text">
                            <div><?php the_content(); ?></div>            
                        </div>
			        </article>
			    <?php endwhile; ?>
			    <?php else: ?>
			    	<p>NO EXISTE NINGÚN POST</p>
				<?php endif; ?>
			<?php rewind_posts(); ?>
		</div>
	</div>
</section>
<script>
    $(document).ready(function() {
        MAIN.utils.home();
    });
</script>
<?php get_template_part('include/footer'); ?>