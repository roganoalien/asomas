<?php get_template_part('include/header-page'); ?>
<?php while( have_posts() ) : the_post(); ?>
    <?php $thumb = wp_get_attachment_url( get_post_thumbnail_id($post->ID)); ?>
    <?php $title = get_the_title(); ?>
    <?php
        if($title == 'Galería'){
            $title = 'galeria';
        }
    ?>
    <section class="landing-page" style="background: url(<?php echo $thumb; ?>) no-repeat center center;">
        <div class="container">
            <div class="row">
                <article class="landing-page-info col s12">
                    <h1><?php echo $title ?></h1>
                    <p><?php the_excerpt(); ?></p>
                </article>
            </div>
        </div>
    </section>
    <section class="page-content <?php echo strtolower($title); ?>">
        <div class="container">
            <div class="row">
                <article>
                    <?php the_content(); ?>
                </article>
            </div>
        </div>
    </section>
<?php endwhile; ?>
<?php get_template_part('include/footer'); ?>